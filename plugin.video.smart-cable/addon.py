import sys
import os

REMOTE_DBG = False

# append pydev remote debugger
if REMOTE_DBG:
    # Make pydev debugger works for auto reload.
    # Note pydevd module need to be copied in XBMC\system\python\Lib\pysrc
    try:
        sys.path.append("C:\\Users\\aman\\AppData\\Roaming\\Appcelerator\\Aptana Studio\\plugins\\org.python.pydev_3.0.0.1388187472\\pysrc")
        import pydevd # with the addon script.module.pydevd, only use `import pydevd`
    # stdoutToServer and stderrToServer redirect stdout and stderr to eclipse console
        pydevd.settrace('localhost', stdoutToServer=True, stderrToServer=True)
    except ImportError:
        sys.stderr.write("Error: " +
            "You must add org.python.pydev.debug.pysrc to your PYTHONPATH.")
        sys.exit(1)

################### Path setup #################################
main_path = str(os.path.dirname(os.path.realpath(__file__)))
resource_path = main_path + '/resources'
lib_path = resource_path + '/lib'
media_info_path = resource_path + '/mediainfo'
sys.path.append(resource_path)
sys.path.append(lib_path)
sys.path.append(media_info_path)



class Main:
  def __init__(self):
    self.pDialog = None
    self.curr_file = ''
    self.run()

  def run(self):
    import mainapp as main
    main.MainApp()

app_window = Main()