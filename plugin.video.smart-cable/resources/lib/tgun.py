'''
Created on 2014-04-10

@author: masinko
'''
import commonutils as common
from appmanager import AppManager
from appmanager import Mode
import os

class TGun:
    
    _main_url = ''
    _name = 'Live TV Show'
    _icon_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../icons/tgun/'
    _fanart_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../fanarts/tgun/'
    _swf_url = 'http://www.tgun.tv/locked/player.swf'

    def __init__(self,app_manager):
       media_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../mediainfo/tgun/'
       cfg_file = open(media_path + 'tgun.cfg', 'r')
       self.tgun_media = cfg_file.read()
       cfg_file.close()
       self.app = app_manager
       
    def getName(self):
        return TGun._name
    
    def getIcon(self):
        return TGun._icon_path + 'main.png'
    
    def getNextPageIcon(self):
        return TGun._icon_path + 'nextpage.png'
       
    def getFanart(self):
        return TGun._fanart_path + 'main.jpg'
    
    def getCategoryFanart(self):
        return TGun._fanart_path + 'categoryfanart.jpg'
         
    
    def displayMainMenu(self):
        match = common.find('(.+?)\s*(rtmp:.+?)\s*(http:.+?)\s', self.tgun_media)
        for item in match:
            title = item[0]
            rtmp = item[1]
            page_url = item[2]
            start = page_url.index('?') + 1
            play_path = page_url[start:]
            videoLink = rtmp + ' playpath='+play_path + ' swfUrl='+TGun._swf_url + ' live=true pageUrl='+page_url
            self.app.getAddon().add_video_item({'mode': Mode.PLAY, 'url': videoLink}, {'title': title}, img=self.getIcon(),fanart=self.getFanart())

