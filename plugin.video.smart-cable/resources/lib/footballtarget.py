'''
Created on 2014-04-10

@author: masinko
'''
import commonutils as common
from appmanager import AppManager
from appmanager import Mode
import os
from BeautifulSoup import BeautifulSoup, SoupStrainer
import re

class FootBallTarget:
    
    _main_url = 'http://www.footballtarget.com/'
    _name = 'FootBall Target'
    _icon_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../icons/tgun/'
    _fanart_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../fanarts/tgun/'

    def __init__(self,app_manager):
       self.app = app_manager
       
    def getName(self):
        return FootBallTarget._name
    
    def getIcon(self):
        return FootBallTarget._icon_path + 'main.png'
    
    def getNextPageIcon(self):
        return FootBallTarget._icon_path + 'nextpage.png'
       
    def getFanart(self):
        return FootBallTarget._fanart_path + 'main.jpg'
    
    def getCategoryFanart(self):
        return FootBallTarget._fanart_path + 'categoryfanart.jpg'

    def displayMainMenu(self):
        main_menu = ([('HIGHLIGHTS','latest*highlights'),
                     ('FULL MATCHES','full-match-replay-video'),
                     ('GOALS','golazo'),
                     ('LEAGUES','premiership*liga-bbva*bundesliga*seriea*france*champions-league*uefaleague*')])
        for menu in main_menu:
            link = menu[1]
            category = menu[0]
            self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': link, 'level': '0'}, {'title': category}, img=self.getIcon(),fanart=self.getCategoryFanart())

    def displaySubMenu(self):
        level = self.app.getAddonQueries('level')
        url = self.app.getAddonQueries('url')
        if level == '0' :
            items = str(url).split('*')
            for item in items:
                title = str(item).upper()
                self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': item, 'level': '1'}, {'title': title}, img=self.getIcon(),fanart=self.getCategoryFanart())
        elif level == '1' :
            url = FootBallTarget._main_url + url + '/'
            total_pages = self.getTotalPages(url)
            self.populateMediaList(url, 1, total_pages)
        elif level == '2' :
            video_links = self.getVideoLink(url)
            for item in video_links:
                self.app.getAddon().add_video_item({'mode': Mode.PLAY, 'name': self.getName(), 'url': item, 'resolved': 'true'}, {'title': item}, img=self.getIcon(),fanart=self.getCategoryFanart())
            #for item in items:
                #self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': item, 'level': '1'}, {'title': item}, img=self.getIcon(),fanart=self.getCategoryFanart())

    def displayNextPage(self):
        page_num = self.app.getAddonQueries('page_num')
        url = self.app.getAddonQueries('url')
        cur_page = int(page_num) + 1
        page_url = url+'page/'+str(cur_page)+'/'
        print 'page url is -----------'
        print page_url
        total_pages = self.app.getAddonQueries('total_pages')
        self.populateMediaList(page_url,cur_page,total_pages)
        #if int(cur_page) < int(total_pages):
            #self.app.getAddon().add_directory({'mode': Mode.NEXT, 'name': self.getName(), 'url': page_url, 'page_num': cur_page, 'total_pages': total_pages}, {'title': str(cur_page) + ' of ' + str(total_pages)+ ' Next Page >>>'}, img=self.getNextPageIcon())

    
    def populateMediaList(self,url,curPage,totalPages):
        page_src = common.urlReader(url)
        outermosttag = SoupStrainer("div",{"id": "archive-area"})
        menu = BeautifulSoup(page_src, parseOnlyThese=outermosttag)
        menutags = SoupStrainer("li",{"class": "cat-blog-container"})
        for tag in BeautifulSoup(str(menu), parseOnlyThese=menutags):
            ahref = tag.find('a')
            link = ahref.get('href')
            title = str(link).replace(FootBallTarget._main_url,'')
            icon = 'http://cdn.footballtarget.com/wp-content/uploads/2013/04/footballtargetvideo133.jpg'
            #title = link
            self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': link, 'level': '2'}, {'title': title}, img=icon,fanart=self.getCategoryFanart())
        if int(curPage) < int(totalPages):
            self.app.getAddon().add_directory({'mode': Mode.NEXT, 'name': self.getName(), 'url':url, 'page_num': curPage, 'total_pages': totalPages}, {'title': str(curPage) + ' of ' + str(totalPages)+ ' Next Page >>>'}, img=self.getIcon())

    
    def getVideoLink(self,url):
        print 'video link url is-------->'
        print url
        page_src = common.urlReader(url)
        video_links = []
        type = str(url).replace(FootBallTarget._main_url,'').split('/')[2]
        if (str(type).startswith('full')):
            video_links = self.getFullMatchVideoLink(url)
        else:
            video_links = self.getNonFullMatchVideoLink(page_src)
        return video_links

    def getNonFullMatchVideoLink(self,src):
        video_links = []
        menus = SoupStrainer("script",{"data-publisher-id": "21772"})
        for tag in BeautifulSoup(src, parseOnlyThese=menus):
            video_id = common.find('.*data-video-id="(.+?)"></script>',str(tag))[0]
            #if this addon breaks look at this line
            video_link = 'http://cdn.phoenix.intergi.com/21772/videos/'+video_id+'/video-sd.mp4'
            video_links.append(video_link)
        if not video_links:
            video_links = self.getVideoLinkB(src)
        return video_links

    def getFullMatchVideoLink(self,url):
        print 'video link url is-------->'
        print url
        page_src = common.urlReader(url)
        video_links = []
        outermenu = SoupStrainer("div",{"id": "content-area"})
        pagemenu = SoupStrainer("span",{"class": "_text"})
        outertag = BeautifulSoup(page_src, parseOnlyThese=outermenu)
        pagetag = BeautifulSoup(str(outertag), parseOnlyThese=pagemenu)
        video_links = self.getVideoLinkB(page_src)
        if not len(pagetag) == 0:
            pagenum = str(pagetag).replace('<span class="_text">','').split('</span>')[0].split(' ')[-1]
            url = url + pagenum + '/'
            page_src = common.urlReader(url)
            video_links1 = self.getVideoLinkB(page_src)
            video_links = video_links + video_links1
        return video_links

    def getVideoLinkB(self,src):
        video_links = []
        outermenu = SoupStrainer("div",{"id": "content-area"})
        outertag = BeautifulSoup(src, parseOnlyThese=outermenu)
        innermenu = SoupStrainer("script",{"type": "text/javascript"})
        for tag in BeautifulSoup(str(outertag), parseOnlyThese=innermenu):
            try:
                print '--------------tag------------' 
                print tag.get('data-config')
                jasonFile = common.urlReader('http:'+str(tag.get('data-config')))
                xmlFile = common.find('\"f4m\":\"(.+?)\"',str(jasonFile))[0]
                half_link= common.find('<baseURL>(.+?)</baseURL>',common.urlReader(xmlFile))[0]
                #if this addon breaks look at this line
                video_link = str(half_link) + '/video-sd.mp4'
                video_links.append(video_link)
            except:
                print 'exception'
        return video_links

    def getVideoLinkC(self,src):
        video_links = []
        outermenu = SoupStrainer("div",{"id": "content-area"})
        outertag = BeautifulSoup(src, parseOnlyThese=outermenu)
        innermenu = SoupStrainer("script",{"type": "text/javascript"})
        for tag in BeautifulSoup(str(outertag), parseOnlyThese=innermenu):
            try:
                print '--------------tag------------' 
                print tag.get('data-config')
                video_info = common.find('data-config="(.+?).json".+?',str(tag))
                video_info = str(video_info[0]).replace('http://config.playwire.com/','').split('/')
                video_id = video_info[3]
                host_id = video_info[0]
                print video_id
                #if this addon breaks look at this line
                video_link = 'http://cdn.phoenix.intergi.com/'+ host_id + '/videos/' + video_id + '/video-sd.mp4'
                video_links.append(video_link)
            except:
                print 'exception'
        return video_links

    def getTotalPages(self,url):
        page_src = common.urlReader(url)
        menus = SoupStrainer("div",{"class": "pagination"})
        tags = BeautifulSoup(page_src, parseOnlyThese=menus)
        page_info = tags.findAll('span')
        print page_info
        return str(page_info[0].text).split(' ')[-1]
        
    def resolve(self,url):
        video_link = common.getResolvedLink(url)
        print video_link
        return video_link