




from t0mm0.common.addon import Addon
import urlresolver
import urllib2
import urllib
from appmanager import AppManager
from appmanager import Mode

################### Source Import #################################
from diretube import DireTube
from amharicmovies import AmharicMovies
from tgun import TGun
from ethiocinema import EthioCinema
from watchseriesus import WatchSeriesUS
from motd import MOTD
from footballtarget import FootBallTarget

class MainApp():

    _media_list = []

    def __init__(self):
        self.app = AppManager()
        self.populateMediaList()
        self.play = self.app.getAddonQueries('play')
        self.mode = self.app.getAddonQueries('mode')
        self.selected_media = self.getSelectedMedia()
        url = self.app.getAddonQueries('url')
        print 'Mode: ' + str(self.mode)
        print 'Play: ' + str(self.play)
        print 'URL: ' + str(url)
        self.run()
        #print 'Testing PlayWire'
        #self.playTest()
        
    def run(self):        
        if self.mode == 'main':
            self.displayMainMenu()
        else:
            self.handleMode()          
        if not self.play:
            self.app.endAddingDirectory()
        else:
            resolved = str(self.app.getAddonQueries('resolved')).strip()
            media_link = self.app.getURL()
            if resolved == 'false':
                print '---------resolving'
                print self.getSelectedMedia()
                media_link = self.selected_media.resolve(media_link)
                print 'media-------'
                print media_link
            self.app.getAddon().resolve_url(media_link)
            
    def handleMode(self):
        if self.mode == str(Mode.MAINMENUSELECTED):
            print 'Media Selected'
            self.selected_media.displayMainMenu()            
        elif self.mode == str(Mode.SUBMENUSELECTED):
            print 'item selected'
            self.selected_media.displaySubMenu()            
        elif self.mode == str(Mode.NEXT):
            print 'item selected'
            self.selected_media.displayNextPage()   
        print "im in handle mode"
       
    def getSelectedMedia(self):
        print 'getting selected media'
        print 'number of medias'
        print len(MainApp._media_list)
        for media in MainApp._media_list:
            if str(media.getName()) == str(self.app.getName()):
                print 'found selected media'
                print media.getName()
                return media

    def displayMainMenu(self):
        for each_media in MainApp._media_list:
            self.app.getAddon().add_directory({'mode': Mode.MAINMENUSELECTED,'name':each_media.getName()}, {'title': each_media.getName()},img=each_media.getIcon(),fanart=each_media.getFanart(),total_items=len(MainApp._media_list))
        self.playTest()

    def populateMediaList(self):
        MainApp._media_list.append(DireTube(self.app))
        MainApp._media_list.append(AmharicMovies(self.app))
        MainApp._media_list.append(TGun(self.app))
        MainApp._media_list.append(EthioCinema(self.app))
        MainApp._media_list.append(WatchSeriesUS(self.app))
        MainApp._media_list.append(MOTD(self.app))
        MainApp._media_list.append(FootBallTarget(self.app))

    def playTest(self):
        url = "http://www.dailymotion.com/video/k5fSMUAG4JiavCanepx"
        print url
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
        streamlink = urlresolver.resolve(urllib2.urlopen(req).url)
        print '**************************playTest*********************'
        print streamlink
        self.app.getAddon().resolve_url(streamlink)

