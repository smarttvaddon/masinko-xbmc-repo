'''
Created on 2014-04-10

@author: masinko
'''
import re
import urllib2
import urlresolver
from BeautifulSoup import BeautifulSoup


def getResolvedLink(url):
    req = urllib2.Request(url)
    #req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
    req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0')
    streamlink = urlresolver.resolve(urllib2.urlopen(req).url)
    return streamlink

def fileReader(src,starttext,endtext,length):
    data = []
    start = 0
    end = 0
    length = 105
    with open (src, "r",encoding="utf8") as myfile:
        for i, line in enumerate(myfile, 1):
            if starttext in line:
                start = i
                if length != 0:
                    end = start + length
                else:
                    if endtext in line:
                        end = i
            if start > 0 and i >= start and i < end and end > 0  : 
                data.append(line)
    return ' '.join(data)

def urlReader(url):
    req = urllib2.Request(url)
    req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0')
    response = urllib2.urlopen(req)
    urlsource = response.read().decode("ISO-8859-1")
    response.close()
    return urlsource

def extractSubString(string,startstr,endstr,start=0):
    startindex = string.find(startstr,start)
    endindex = string.find(endstr,startindex)
    substring = string[startindex:endindex]
    return substring

def find(regEx,src):
    return re.compile(regEx).findall(src)

def removeNonASCII(text):
    return ''.join([i if ord(i) < 128 else ' ' for i in text])

def getSoup(url):
    main_src = urlReader(url)
    soup = BeautifulSoup(main_src)
    return soup

# sets the colours for the lists
def coloring(text , color , colorword ):
    if color == "white":
        color = "FFFFFFFF"
    if color == "blue":
        color = "FF0000FF"
    if color == "cyan":
        color = "FF00FFFF"
    if color == "violet":
        color = "FFEE82EE"
    if color == "pink":
        color = "FFFF1493"
    if color == "red":
        color = "FFFF0000"
    if color == "green":
        color = "FF00FF00"
    if color == "yellow":
        color = "FFFFFF00"
    if color == "orange":
        color = "FFFF4500"
    colored_text = text.replace( colorword , "[COLOR=%s]%s[/COLOR]" % ( color , colorword ) )
    return colored_text

# sets the bold or italic values for the lists
# usage: format('Hello World','bold', 'World')
def format(text , fmt_value , fmt_part ):
    if fmt_value == "bold":
        fmt_regex = "[B]%s[/B]"
    if fmt_value == "italic":
        fmt_regex = "[i]%s[/i]"
    if fmt_value == "upper":
        fmt_regex = "[UPPERCASE]%s[/UPPERCASE]"
    if fmt_value == "lower":
        fmt_regex = "[LOWERCASE]%s[/LOWERCASE]"
    formatted_text = text.replace( fmt_part , fmt_regex  % ( fmt_part ) )
    return formatted_text
        

if __name__ == '__main__':
    pass