
import sys
import xbmcgui
import xbmcplugin
import xbmcaddon
import re, string
import os
import urllib2

from t0mm0.common.addon import Addon

class Mode:
    MAINMENUSELECTED = 1
    SUBMENUSELECTED  = 2
    PLAY = 3
    NEXT = 4
    ADDTOFAVOURITES = 5
    REMOVEFROMFAVOURITES = 6


class AppManager():
    
    def __init__(self):
        self.plugin_name = 'plugin.video.smart-cable'
        self.addon = Addon(self.plugin_name, sys.argv)
        self.xaddon = xbmcaddon.Addon(id=self.plugin_name)
        self.datapath = self.addon.get_profile()
        
    def getAddonQueries(self,info):
        if info != 'mode':
            return self.addon.queries.get(info,None)
        else:
            return self.addon.queries['mode']
        
    def getURL(self):
        return self.getAddonQueries('url')
    
    def getName(self):
        return self.getAddonQueries('name')
        
    def getAddon(self):
        return self.addon
    
    def addDirectory(self,queries, infolabels, img, fanart, total_items,is_folder=True):
        self.addon.add_directory(queries, infolabels, img, fanart,total_items,is_folder)
        
    def endAddingDirectory(self):
        self.addon.end_of_directory()