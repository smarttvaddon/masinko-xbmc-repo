'''
Created on 2014-04-10

@author: masinko
'''
import commonutils as common
from appmanager import AppManager
from appmanager import Mode
import os
from BeautifulSoup import BeautifulSoup, SoupStrainer
import re

class MOTD:
    
    _main_url = 'http://motdstream.com/category/video/'
    _name = 'MOTD'
    _icon_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../icons/tgun/'
    _fanart_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../fanarts/tgun/'

    def __init__(self,app_manager):
       self.app = app_manager
       
    def getName(self):
        return MOTD._name
    
    def getIcon(self):
        return MOTD._icon_path + 'main.png'
    
    def getNextPageIcon(self):
        return MOTD._icon_path + 'nextpage.png'
       
    def getFanart(self):
        return MOTD._fanart_path + 'main.jpg'
    
    def getCategoryFanart(self):
        return MOTD._fanart_path + 'categoryfanart.jpg'

    def displayMainMenu(self):
        total_pages = self.getTotalPages(MOTD._main_url)
        self.populateMediaList(MOTD._main_url, 1, total_pages)

    def displaySubMenu(self):
        level = int(self.app.getAddonQueries('level'))
        url = self.app.getAddonQueries('url')
        print 'url and levels are ----->>>'+ url + "--" + str(level)
        video_links = self.getVideoLink(url)
        print 'video_links ----->>>'+ str(video_links)
        for item in video_links:
            title = common.removeNonASCII(str(item))
            self.app.getAddon().add_video_item({'mode': Mode.PLAY, 'name': self.getName(), 'url': item, 'resolved': 'true'}, {'title': title}, img=self.getIcon(),fanart=self.getCategoryFanart())

    def displayNextPage(self):
        page_num = self.app.getAddonQueries('page_num')
        cur_page = int(page_num) + 1
        page_url = MOTD._main_url+'page/'+str(cur_page)+'/'
        print 'page url is -----------'
        print page_url
        total_pages = self.app.getAddonQueries('total_pages')
        self.populateMediaList(page_url,cur_page,total_pages)
        #if int(cur_page) < int(total_pages):
            #self.app.getAddon().add_directory({'mode': Mode.NEXT, 'name': self.getName(), 'url': page_url, 'page_num': cur_page, 'total_pages': total_pages}, {'title': str(cur_page) + ' of ' + str(total_pages)+ ' Next Page >>>'}, img=self.getNextPageIcon())

    
    def populateMediaList(self,url,curPage,totalPages):
        page_src = common.urlReader(url)
        menus = SoupStrainer("div",{"class": "cover"})
        for tag in BeautifulSoup(page_src, parseOnlyThese=menus):
            link = tag.find('a').get('href')
            title = str(link).replace('http://motdstream.com/','')[:-1]#common.removeNonASCII(str(tag.find('a').get('title'))).strip()
            icon = tag.find('img').get('src')
            self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': link, 'level': '0'}, {'title': title}, img=icon,fanart=self.getCategoryFanart())
        if int(curPage) < int(totalPages):
            self.app.getAddon().add_directory({'mode': Mode.NEXT, 'name': self.getName(), 'page_num': curPage, 'total_pages': totalPages}, {'title': str(curPage) + ' of ' + str(totalPages)+ ' Next Page >>>'}, img=self.getIcon())
    
    
    def getVideoLink(self,url):
        page_src = common.urlReader(url)
        video_links = []
        menus = SoupStrainer("script",{"data-publisher-id": "21772"})
        for tag in BeautifulSoup(page_src, parseOnlyThese=menus):
            video_id = common.find('.*data-video-id="(.+?)"></script>',str(tag))[0]
            #if this addon breaks look at this line
            video_link = 'http://cdn.phoenix.intergi.com/21772/videos/'+video_id+'/video-sd.mp4'
            print '----------------------video-link-----------------------------'
            print video_link
            video_links.append(video_link)
        if not video_links or len(str(video_links[0])) <10:
            video_links = self.getVideoLinkB(page_src)
        if not video_links or len(str(video_links[0])) <10:
            video_links = self.getVideoLinkC(page_src)
        if not video_links or len(str(video_links[0])) <10:
            video_links = self.getVideoLinkD(page_src)
        return video_links

    def getVideoLinkB(self,src):
        video_links = []
        menus = SoupStrainer("div",{"class": "entry"})
        for tag in BeautifulSoup(src, parseOnlyThese=menus):
            try :
                main_link = common.find('src="(.+?)"',str(tag))
                video_link = main_link[0]
                #if this addon breaks look at this line
                video_link = str(video_link).split('?')[0].replace('embed/',"")
                video_link = self.resolve(video_link)
                video_links.append(video_link)
                print video_link
                if not video_links:
                    print 'empty videolink'

            except:
                print '*************video links*************'
                print 'empty videolink'
            
        return video_links

    def getVideoLinkC(self,src):
        video_links = []
        menus = SoupStrainer("div",{"class": "entry"})
        print 'getVideoLinkC is called'
        for tag in BeautifulSoup(src, parseOnlyThese=menus):
            try :
                link = common.find('data-config="(.+?)"',str(tag))
                link = str(link[0]).split('/')
                print 'link is --------->>----->>' + str(link)
                video_id = str(link[-2])
                #if this addon breaks look at this line
                video_link = 'http://cdn.phoenix.intergi.com/21772/videos/'+video_id+'/video-sd.mp4'
                print video_link
                video_links.append(video_link)
                if not video_links:
                    print 'empty videolink'
            except:
                print '*************video links*************'
                print 'Empty'
            
        return video_links

    def getVideoLinkD(self,src):
        video_links = []
        menus = SoupStrainer("embed",{"type": 'video/divx'})
        print 'getVideoLinkC is called'
        for tag in BeautifulSoup(src, parseOnlyThese=menus):
            print str(tag)
            link = str(tag.get('src'))
            print 'link is --------->>----->>' + str(link)
            #if this addon breaks look at this line
            video_link = link
            print video_link
            video_links.append(video_link)
            if not video_links:
                print 'empty videolink'            
        return video_links
        
    def getTotalPages(self,url):
        page_src = common.urlReader(url)
        menus = SoupStrainer("a",{"class": "page-numbers"})
        tags = BeautifulSoup(page_src, parseOnlyThese=menus)
        page_num = 0
        for link in tags.findAll('a'):
            if int(link.text) > page_num:
                page_num = int(link.text)
        return page_num
        
    def resolve(self,url):
        video_link = common.getResolvedLink(url)
        print video_link
        return video_link