'''
Created on 2014-04-10

@author: masinko
'''
import commonutils as common
from appmanager import AppManager
from appmanager import Mode
import os
from BeautifulSoup import BeautifulSoup, SoupStrainer
import re

class DireTube:
    
    _main_url = ''
    _name = 'DireTube'
    _icon_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../icons/diretube/'
    _fanart_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../fanarts/diretube/'

    def __init__(self,app_manager):
       _main_url = 'http://www.diretube.com/index.html'
       self.app = app_manager
       
    def getName(self):
        return DireTube._name
    
    def getIcon(self):
        return DireTube._icon_path + 'main.png'
    
    def getNextPageIcon(self):
        return DireTube._icon_path + 'nextpage.png'
       
    def getFanart(self):
        return DireTube._fanart_path + 'main.jpg'
    
    def getCategoryFanart(self):
        return DireTube._fanart_path + 'categoryfanart.jpg'
    
    def getSubMenuFanart(self):
        return DireTube._fanart_path + 'submenufanart.jpg'
        
    
    def displayMainMenu(self):
        main_menu = [('category','http://www.diretube.com'),('Top Videos','http://www.diretube.com/topvideos.html'),('New Videos','http://www.diretube.com/newvideos.html')]
        for menu in main_menu:
            link = menu[1]
            category = menu[0]
            self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': link, 'categoryPage': link, 'category': category}, {'title': category}, img=self.getIcon(),fanart=self.getCategoryFanart())
            
    def displaySubMenu(self):
        category = str(self.app.getAddonQueries('category')).strip()
        if category == 'category':
            self.populateCategoryList()
        else:
            page_url = self.app.getURL()
            self.populateMediaList(page_url)
            total_pages = self.getNumberOfPagesForCategory(page_url)
            self.app.getAddon().add_directory({'mode': Mode.NEXT, 'name': self.getName(), 'url': page_url, 'page_num': 1, 'total_pages': total_pages}, {'title': '1 of ' + str(total_pages)+ ' Next Page >>>'}, img=self.getNextPageIcon())
           
    def displayNextPage(self):
        page_num = self.app.getAddonQueries('page_num')
        cur_page = int(page_num) + 1
        url = self.app.getURL()
        page_url = url[:-11] + str(cur_page) + '-date.html'
        print 'page url is -----------'
        print page_url
        self.populateMediaList(page_url)
        total_pages = self.app.getAddonQueries('total_pages')
        if int(cur_page) < int(total_pages):
            self.app.getAddon().add_directory({'mode': Mode.NEXT, 'name': self.getName(), 'url': page_url, 'page_num': cur_page, 'total_pages': total_pages}, {'title': str(cur_page) + ' of ' + str(total_pages)+ ' Next Page >>>'}, img=self.getNextPageIcon())
       
    def populateCategoryList(self):
        page_src = common.urlReader('http://www.diretube.com')
        menus = SoupStrainer("ul",{"id": "ul_categories"})
        for tag in BeautifulSoup(page_src, parseOnlyThese=menus):
            link = SoupStrainer('a', href=re.compile('diretube.com'))
            k = BeautifulSoup(str(tag), parseOnlyThese=link)
            for j in k.findAll('a'):
                link = j.get('href')
                category = j.text
                self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': link, 'categoryPage': link, 'category': category, 'page_num': 1}, {'title': category}, img=self.getIcon(),fanart=self.getCategoryFanart())
            

    def getNumberOfPagesForCategory(self,url):
        src = common.urlReader(url)
        match = common.find('<meta name=.*Page(.+?)".* />', src)    
        return int(match[0].split()[-1])
  
    def resolve(self, url):
        page_src = common.urlReader(url)
        print "--------------in resolve--------------------"
        print url
        video_container = SoupStrainer("div",{"id": "detail_page"})
        video_link = ''
        for tag in BeautifulSoup(page_src, parseOnlyThese=video_container):
            link = SoupStrainer("script",{"type": "text/javascript"})
            k = BeautifulSoup(str(tag), parseOnlyThese=link)
            matchLink = common.find('file:\s*\'(.+?)\'', str(k))
            if len(matchLink) > 0:
                video_link = matchLink[0]
                d = 22 - len(video_link)
                if 'http://www.youtube.com' == video_link[:d]:
                    media = urlresolver.HostedMediaFile(video_link)
                    video_link = media.resolve()
        if not video_link:
            video_link = self.resolveB(page_src)
        return video_link

    def resolveB(self, page_src):
        video_container = SoupStrainer("div",{"id": "Playerholder"})
        print "--------------in resolveB--------------------"
        video_link = ''
        for tag in BeautifulSoup(page_src, parseOnlyThese=video_container):
            matchLink = common.find('<embed src="(.+?)".*></embed>',str(tag))
            #print matchLink
            if len(matchLink) > 0:
                video_link = str(matchLink[0]).split("?version")[0]
                video_link = common.getResolvedLink(video_link)
                print video_link
        return video_link

    def populateMediaList(self,page_url):
        page_src = common.urlReader(page_url)
        menus = SoupStrainer("div",{"id": "browse_results"})
        prev_link = ''
        for tag in BeautifulSoup(page_src, parseOnlyThese=menus):
            link = SoupStrainer('a', href=re.compile('diretube.com'))
            k = BeautifulSoup(str(tag), parseOnlyThese=link)
            for j in k.findAll('a'):
                #links.append(j.get('href'))
                cur_link = str(j.get('href')).strip()
                if not cur_link == prev_link:
                    img = j.find('img')
                    imgregex = '<img src="(.+?)"\s*.*\s*.*\s*alt="(.+?)"\s*.*/>\s*'
                    match = common.find(imgregex, str(img))
                    print match
                    if len(match) > 0 :
                        icon = match[0][0]
                        title = match[0][1]
                    link = j.get('href')
                    prev_link = cur_link
                    self.app.getAddon().add_video_item({'mode': Mode.PLAY, 'name': self.getName(), 'url': link, 'resolved': 'false'}, {'title': title}, img=icon,fanart=self.getSubMenuFanart())

