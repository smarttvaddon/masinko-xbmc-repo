'''
Created on 2014-04-10

@author: masinko
'''
import commonutils as common
from appmanager import AppManager
from appmanager import Mode
import os
from BeautifulSoup import BeautifulSoup, SoupStrainer
import re

class EthioCinema:
    
    _main_url = 'http://www.ethiocinemareview.com'
    _name = 'EthioCinema'
    _icon_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../icons/ethiocinema/'
    _fanart_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../fanarts/ethiocinema/'
    _media_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../mediainfo/ethiocinema/'
    _swf_url = 'http://www.tgun.tv/locked/player.swf'

    def __init__(self,app_manager):
       self.app = app_manager
       
    def getName(self):
        return EthioCinema._name
    
    def getIcon(self):
        return EthioCinema._icon_path + 'main.png'
    
    def getNextPageIcon(self):
        return EthioCinema._icon_path + 'nextpage.png'
       
    def getFanart(self):
        return EthioCinema._fanart_path + 'main.jpg'
    
    def getCategoryFanart(self):
        return EthioCinema._fanart_path + 'categoryfanart.jpg'
         
    
    def displayMainMenu(self):
        with open(EthioCinema._media_path + 'mainmenu.cfg', 'r') as f:
            for line in f:
                if not str(line).startswith('http') and not len(line) < 2:
                    self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'category': line, 'level': '0'}, {'title': line}, img=self.getIcon(),fanart=self.getFanart())
           
    def displaySubMenu(self):
        level = int(self.app.getAddonQueries('level'))
        category = str(self.app.getAddonQueries('category')).strip()
        if level == 0:
            with open(EthioCinema._media_path + 'mainmenu.cfg', 'r') as f:
                for line in f:
                    if str(line).startswith('http') and not len(line) < 2:
                        links = line.split('/')
                        if str(links[-3]) == category:
                            title = links[-2]
                            menu_url = line
                            self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': menu_url, 'level': '1'}, {'title': title}, img=self.getIcon(),fanart=self.getFanart())
                        elif str(links[-3]) == 'category' and str(links[-2]) == category :
                            title = links[-2]
                            menu_url = line
                            self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': menu_url, 'level': '1'}, {'title': title}, img=self.getIcon(),fanart=self.getFanart())
        
        if level == 1:
            page_url = self.app.getURL()
            print '---------------'
            print page_url
            total_pages = self.populateMediaList(page_url)
            self.app.getAddon().add_directory({'mode': Mode.NEXT, 'name': self.getName(), 'url': page_url, 'page_num': 1, 'total_pages': total_pages}, {'title': '1 of ' + str(total_pages)+ ' Next Page >>>'}, img=self.getNextPageIcon())
            
        if level == 2:
            print '------------level 2'
            page_url = str(self.app.getURL()).strip()
            self.populateParts(page_url)
            
    def displayNextPage(self):
        page_num = self.app.getAddonQueries('page_num')
        cur_page = int(page_num) + 1
        url = str(self.app.getURL()).strip()
        page_url = ''
        if str(url[:-2]).endswith('page/'):
            page_url = url[:-2] + str(cur_page) + '/'
        else:
            page_url = url + 'page/' + str(cur_page) + '/' 
        total_pages = self.app.getAddonQueries('total_pages')
        self.populateMediaList(page_url)
        self.app.getAddon().add_directory({'mode': Mode.NEXT, 'name': self.getName(), 'url': page_url, 'page_num': cur_page, 'total_pages': total_pages}, {'title': str(cur_page) + ' of ' + str(total_pages)+ ' Next Page >>>'}, img=self.getNextPageIcon())
               
    def populateMediaList(self,page_url):
        print page_url
        total_pages = self.getTotalPages(page_url)
        page_src = common.urlReader(page_url)
        page_regex = page_url+'page/'
        page_links = SoupStrainer("div",{"class": "cat-news"})
        for tag in BeautifulSoup(page_src, parseOnlyThese=page_links):
            h3 = SoupStrainer('h3')
            ratings_tag = SoupStrainer("div",{"class": "hnews-rating"})
            tag2 = BeautifulSoup(str(tag), parseOnlyThese=ratings_tag)
            ratings_info = tag2.findAll('strong')
            index = 0
            for tag2 in BeautifulSoup(str(tag), parseOnlyThese=h3):
                votes = 'N/A'
                ratings = 'N/A'
                if len(ratings_info) > index+1 :
                    votes = ratings_info[index].text
                    ratings = ratings_info[index+1].text
                title = common.removeNonASCII(tag2.find('a').text)
                if common.find('&#\d*',title):
                    title = re.sub(r'(&#\d*)', r'', title)
                url = tag2.find('a').get('href')
                img = SoupStrainer('img', alt=re.compile(title))
                tag3 = BeautifulSoup(str(tag), parseOnlyThese=img)
                regex = '<img\s*.*src="(.+?)"\s*.*\s*/>.*\s*'
                match = common.find(regex, str(tag3))
                icon = self.getIcon()
                title = title + common.coloring('  VOTES:'+votes,'red', 'VOTES:'+votes) + common.coloring('   RATINGS:'+ratings + '/5','blue', '   RATINGS:'+ratings + '/5')
                index += 2
                if len(match) > 0 :
                    icon = match[0]
                self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': url, 'level': '2'}, {'title': title }, img=icon,fanart=self.getFanart())
        return total_pages
           
    def populateParts(self,page_url):
        part = 1       
        page_src = common.urlReader(page_url)
        page_regex = page_url+'page/'
        page_links = SoupStrainer("div",{"class": "itemIntroText"})
        for tag in BeautifulSoup(page_src, parseOnlyThese=page_links):
            link = SoupStrainer('a', href=re.compile('amharicmovies.com'))
            link2 = SoupStrainer('a', href=re.compile('ethioscoop.com'))
            k = BeautifulSoup(str(tag), parseOnlyThese=link)
            if len(str(k)) < 1 :
                print 'trying ethioscoop link'
                k = BeautifulSoup(str(tag), parseOnlyThese=link2)
            for j in k.findAll('a'):
                video_link = j.get('href')
                title = 'Part ' + str(part)
                part = part + 1
                self.app.getAddon().add_video_item({'mode': Mode.PLAY, 'name': self.getName(), 'url': video_link, 'resolved':'false'}, {'title': title}, img=self.getIcon(),fanart=self.getFanart())


        
    def resolve(self,url):
        link1 = 'http://www.amharicmovies.com/'
        link2 = 'http://www.ethioscoop.com/'
        if url.startswith(link1) == True or url.startswith(link2) == True:
            link = ''
            video_links = common.getSoup(url).findAll('script', {'type': ['text/javascript']})
            for l in video_links:
                p = common.find('file:\s*\'(.+?)\'', str(l))
                if len(p)> 0:
                    link = str(p[0]).strip()
                    break
            return link

    
    def getTotalPages(self,page_url):
        page_src = common.urlReader(page_url)
        page_regex = str(page_url).strip() + 'page/'
        page_links = SoupStrainer('a', href=re.compile(page_regex))
        match = []
        for tag in BeautifulSoup(page_src, parseOnlyThese=page_links):
            regex = '<a.*.\s*.*href="' + page_regex + '(.+?)/".*\s*'
            match.append(common.find(regex, str(tag)))
        if len(match) > 0 :
            return match[-1][0]

    def getImageList(self,soup):
        img_list = []
        for page_row in soup.findAll('div', {'class': ['row']}):
            left_col = page_row.findAll('div', {'class': ['inner_featured']})
            for s in left_col:
                for s1 in s.findAll('img'):
                    img_list.append(common.find('.*\s*src="(.+?)"', str(s1))[0])
        return img_list
    
    def getLinkTitle(self,soup):
        info = []
        for page_row in soup.findAll('div', {'class': ['row']}):
            right_col = page_row.findAll('h2', {'class': ['title']})          
            for item in right_col:
                if len(item) > 0 :
                    for link in item.findAll('a'):
                        menu_name = link.text
                        menu_url = link.get('href')
                        info.append((menu_name,menu_url))
                        #print((menu_name,menu_url))
        return info
    
    def joinImageLinkTitle(self,soup):    
        i = self.getImageList(soup)
        l = self.getLinkTitle(soup)
        res = []
        index = 0
        for item in l:
            item = list(item)
            item.append(i[index])
            index = index + 1
            res.append(tuple(item))
        
        resfinall = []
        for j in res:
            if not resfinall.count(j) > 0:
                resfinall.append(j)
        return resfinall

            
            