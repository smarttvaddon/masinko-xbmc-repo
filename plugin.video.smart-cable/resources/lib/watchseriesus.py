'''
Created on 2014-04-10

@author: masinko
'''
import commonutils as common
from appmanager import AppManager
from appmanager import Mode
import os
from BeautifulSoup import BeautifulSoup, SoupStrainer
import re

class WatchSeriesUS:
    
    _main_url = 'http://watchseriesus.tv/'
    _name = 'Watch Series'
    _icon_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../icons/tgun/'
    _fanart_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../fanarts/tgun/'
    _swf_url = 'http://www.tgun.tv/locked/player.swf'

    def __init__(self,app_manager):
       media_path = str(os.path.dirname(os.path.realpath(__file__))) + '/../mediainfo/tgun/'
       cfg_file = open(media_path + 'tgun.cfg', 'r')
       self.tgun_media = cfg_file.read()
       cfg_file.close()
       self.app = app_manager
       
    def getName(self):
        return WatchSeriesUS._name
    
    def getIcon(self):
        return WatchSeriesUS._icon_path + 'main.png'
    
    def getNextPageIcon(self):
        return WatchSeriesUS._icon_path + 'nextpage.png'
       
    def getFanart(self):
        return WatchSeriesUS._fanart_path + 'main.jpg'
    
    def getCategoryFanart(self):
        return WatchSeriesUS._fanart_path + 'categoryfanart.jpg'

    def displayMainMenu(self):
        main_menu = [('TV Shows','http://watchseriesus.tv/category/tv-packs/'),('Popular Shows','http://watchseriesus.tv/popular-new/'),('Last 350 post','http://watchseriesus.tv/last-350-post/')]
        for menu in main_menu:
            link = menu[1]
            category = menu[0]
            self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': link, 'category': category, 'level': '0'}, {'title': category}, img=self.getIcon(),fanart=self.getCategoryFanart())

    def displaySubMenu(self):
        level = int(self.app.getAddonQueries('level'))
        url = self.app.getAddonQueries('url')
        page_src = common.urlReader(url)
        if self.app.getAddonQueries('category') == 'TV Shows':
            if level == 0:
                AZ = ['#','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z']
                for item in AZ:
                    self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': url, 'category': 'TV Shows', 'subcategory': item, 'level': '1'}, {'title': item}, img=self.getIcon(),fanart=self.getCategoryFanart())
            if level == 1:
                menus = SoupStrainer("div",{"class": "sidebar-right"})
                tags = BeautifulSoup(page_src, parseOnlyThese=menus)
                for link in tags.findAll('a'):
                    show_name = common.removeNonASCII(link.text).strip()
                    show_url = str(link.get('href')).strip()
                    sub_category = self.app.getAddonQueries('subcategory')
                    if sub_category == '#':
                        if (common.removeNonASCII(link.text).strip()[0].isdigit()):
                            try:
                                self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': show_url, 'category': 'TV Shows', 'level': '2'}, {'title': show_name}, img=self.getIcon(),fanart=self.getCategoryFanart())
                            except:
                                print link
                                print show_name
                    else:
                        if (show_name[0] == sub_category):
                            try:
                                self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': show_url, 'category': 'TV Shows', 'level': '2'}, {'title': show_name}, img=self.getIcon(),fanart=self.getCategoryFanart())
                            except:
                                print link
                                print show_name
            if level == 2:
                menus = SoupStrainer("div",{"class": "wp-pagenavi"})
                tags = BeautifulSoup(page_src, parseOnlyThese=menus)
                page_num = 1
                for link in tags.findAll('a'):
                    page_num = page_num + 1
                i = 1
                while(i <= page_num):
                    if i == 1:
                        page = url
                    else:
                        page = url + 'page/' + str(i) + '/'
                    print 'page number is ' + page
                    filter = SoupStrainer("ul",{"class": "wt-cat"})
                    srcs = BeautifulSoup(common.urlReader(page), parseOnlyThese=filter)
                    for links in srcs.findAll('a'):
                        print links
                        epsiod_title = common.removeNonASCII(links.text).strip()
                        epsiod_url = common.removeNonASCII(links.get('href')).strip()
                        st = url.replace('category/','')[:-1] + '-'
                        epsiod_title = epsiod_url.replace(st, '')[:-1]
                        self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': epsiod_url, 'level': '1'}, {'title': epsiod_title}, img=self.getIcon(),fanart=self.getCategoryFanart())
                    i = i + 1
        else:
            if level == 0:
                menus = SoupStrainer("ul",{"id": "lcp_instance_0"})
                for tag in BeautifulSoup(page_src, parseOnlyThese=menus):
                    link_filter = SoupStrainer('a', href=re.compile('watchseriesus.tv'))
                    links = BeautifulSoup(str(tag), parseOnlyThese=link_filter)
                    for link in links.findAll('a'):
                        cur_link = str(link.get('href')).strip()
                        cur_link_title = common.removeNonASCII(link.get('title'))
                        self.app.getAddon().add_directory({'mode': Mode.SUBMENUSELECTED, 'name': self.getName(), 'url': cur_link, 'level': '1'}, {'title': cur_link_title}, img=self.getIcon(),fanart=self.getCategoryFanart())
            if level == 1:
                menus = SoupStrainer("div",{"class": "filmicerik"})
                tags = BeautifulSoup(page_src, parseOnlyThese=menus)
                #for tag in tags.findAll('p'): for epsiod description todo first to items
                for link in tags.findAll('a'):
                    stream_link = str(link.get('href')).strip()
                    title = str(common.find("http://(.+?)\..*",stream_link)[0])
                    self.app.getAddon().add_video_item({'mode': Mode.PLAY, 'name': self.getName(), 'url': stream_link, 'resolved':'false'}, {'title': title}, img=self.getIcon(),fanart=self.getFanart())

    def resolve(self,url):
        video_link = common.getResolvedLink(url)
        print video_link
        return video_link